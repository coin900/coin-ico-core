package com.coin900.coin.base;

import java.util.HashMap;
import java.util.Map;

/**
 * @author shui
 * @create 2017-11-21
 **/
public class FuConstants {

    public static String ACCESS_KEY = "";
    public static String SECRET_KEY = "";
    public static String BUCKET_NAME = "";
    public static String BUCKET_DOMAIN = "";

    public static String P_ACCESS_KEY = "";
    public static String P_SECRET_KEY = "";
    public static String P_BUCKET_NAME = "";
    public static String P_BUCKET_DOMAIN = "";

    public static String SMS_DY_ACCESS_KEY_ID = "";
    public static String SMS_DY_ACCESS_KEY_SECRET = "";
    public static String SMS_DY_SIGN_NAME = "";
    public static String SMS_DY_TEMPLATE_CODE = "";

    public static String FU_SMS_CODE = "fu_sms_code:";

    // 分页显示
    public static final String FU_PAGE = "page";
    // 众筹项目排序 按照时间
    public static final int FU_PROJECT_ORDERBY_TIME = 1;
    // 众筹项目排序 按照热度
    public static final int FU_PROJECT_ORDERBY_FOLLOW = 2;
    // 首页每个分类显示的项目数量
    public static final int FU_PROJECT_CATEGORY_LIST_NUMBER = 4;
    public static final int FU_PROJECT_CATEGORY_LIST = 5;

    public static final int FU_SAFETY_LEVEL_LOW = 1;
    public static final int FU_SAFETY_LEVEL_MID = 2;
    public static final int FU_SAFETY_LEVEL_HIGH = 3;

    /**
     * 项目进度 项目进度，1众筹中 2众筹完成 3筹资成功 4筹资失败 5下架
     */
    public static final int FU_PROJECT_STATUS_ING = 1;
    public static final int FU_PROJECT_STATUS_DONE = 2;
    public static final int FU_PROJECT_STATUS_SUCCESS = 3;
    public static final int FU_PROJECT_STATUS_FAIL = 4;
    public static final int FU_PROJECT_STATUS_FINISH = 5;

    public static final Map<Integer, String> FU_PROJECT_STATUS = new HashMap<Integer, String>() {
        {
            put(FU_PROJECT_STATUS_ING, "众筹中");
            put(FU_PROJECT_STATUS_DONE, "众筹完成");
            put(FU_PROJECT_STATUS_SUCCESS, "筹资成功");
            put(FU_PROJECT_STATUS_FAIL, "筹资失败");
            put(FU_PROJECT_STATUS_FINISH, "下架");
        }
    };

    /**
     * 项目类型：1参与的项目 2认筹的项目 3关注的项目
     */
    public static final int FU_PROJECT_TYPE_JOIN = 1;
    public static final int FU_PROJECT_TYPE_FUND = 2;
    public static final int FU_PROJECT_TYPE_FOLLOW = 3;

    public static final Map<Integer, String> FU_PROJECT_TYPE = new HashMap<Integer, String>() {
        {
            put(FU_PROJECT_TYPE_JOIN, "参与的项目");
            put(FU_PROJECT_TYPE_FUND, "认筹的项目");
            put(FU_PROJECT_TYPE_FOLLOW, "关注的项目");
        }
    };


    /**
     * 审核状态 0未通过 1通过 2审核中 3已打款
     */
    public static final int FU_CHECK_STATUS_FAIL = 0;
    public static final int FU_CHECK_STATUS_SUCCESS = 1;
    public static final int FU_CHECK_STATUS_ING = 2;
    public static final int FU_CHECK_STATUS_PAYED = 3;

    public static final Map<Integer, String> FU_CHECK_STATUS = new HashMap<Integer, String>() {
        {
            put(FU_CHECK_STATUS_FAIL, "未通过");
            put(FU_CHECK_STATUS_SUCCESS, "通过");
            put(FU_CHECK_STATUS_ING, "审核中");
            put(FU_CHECK_STATUS_PAYED, "已打款");
        }
    };

    /**
     * 订单状态 0未支付 1已支付 2退款 3取消
     */
    public static final int FU_ORDER_STATUS_NOT = 0;
    public static final int FU_ORDER_STATUS_SUCCESS = 1;
    public static final int FU_ORDER_STATUS_REFUND = 2;
    public static final int FU_ORDER_STATUS_CANCEK = 3;

    /**
     * 支付状态 0未支付 1已支付 2审核中 3审核失败
     */
    public static final int FU_PAY_STATUS_NOT = 0;
    public static final int FU_PAY_STATUS_SUCCESS = 1;
    public static final int FU_PAY_STATUS_ING = 2;
    public static final int FU_PAY_STATUS_FAIL = 3;

    /**
     * 支付方式 1支付宝 2微信 3银行卡 4比特币
     */
    public static final int FU_PAY_TYPE_ALIPAY = 1;
    public static final int FU_PAY_TYPE_WX = 2;
    public static final int FU_PAY_TYPE_BANK_CARD = 3;
    public static final int FU_PAY_TYPE_VIRTUAL = 4;

    public static final Map<Integer, String> FU_PAY_TYPE = new HashMap<Integer, String>() {
        {
            put(FU_PAY_TYPE_ALIPAY, "支付宝");
            put(FU_PAY_TYPE_WX, "微信");
            put(FU_PAY_TYPE_BANK_CARD, "银行卡");
            put(FU_PAY_TYPE_VIRTUAL, "比特币");
        }
    };

    /**
     * 获取支付类型名称
     *
     * @param payType
     * @return
     */
    public static String getPayTypeName(Integer payType) {
        Map<Integer, String> fuPayType = FuConstants.FU_PAY_TYPE;
        return fuPayType.get(payType);
    }

    /**
     * 消息类型 1项目
     */
    public static final int FU_MESSAGE_TYPE_PROJECT = 1;
    public static final int FU_MESSAGE_TYPE_USER = 2;

    /**
     * 钱包类型 1银行卡 2虚拟钱包
     */
    public static final int FU_WALLECT_BANK_CARD = 1;
    public static final int FU_WALLECT_VIRTYAL_CURRENCY = 2;

    /**
     * 分销类型 0不开启 1二级分销 2三级分销
     */
    public static final int FU_DISTRIBUTION_CLOSE = 0;
    public static final int FU_DISTRIBUTION_LEVEL_TWO = 1;
    public static final int FU_DISTRIBUTION_LEVEL_THREE = 2;

}

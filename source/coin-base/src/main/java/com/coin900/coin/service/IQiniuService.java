package com.coin900.coin.service;

import com.qiniu.common.QiniuException;
import com.qiniu.storage.model.FileInfo;

import java.io.IOException;

/**
 * Created by laborc on 15/12/24.
 */
public interface IQiniuService {
    public String getUpToken();
    public String getPUpToken();
    public String getDownloadToken(String baseUrl);
    public String getDownloadURL(String url);
    public String getFullUrl(String url);
    public String convertPURL(String htmlTxt);
    public void upload(byte[] data, String key) throws IOException;


}

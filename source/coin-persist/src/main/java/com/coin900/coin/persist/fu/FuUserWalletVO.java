package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * 用户钱包
 *
 * @author shui
 */
public class FuUserWalletVO extends FuVO {

    /**
     * 用户 id
     */
    private Long userId;

    /**
     * 钱包类型 1银行卡 2比特币
     */
    private Integer type;

    /**
     * 名称
     */
    private String name;

    /**
     * 银行卡号/比特币地址
     */
    private String value;

    /**
     * 银行的 id
     */
    private Long bankId;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Long getBankId() {
        return bankId;
    }

    public void setBankId(Long bankId) {
        this.bankId = bankId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuUserWalletVO{" +
                "userId=" + userId +
                ", type=" + type +
                ", name=" + name +
                ", value='" + value + '\'' +
                ", bankId=" + bankId +
                '}';
    }
}
package com.coin900.coin.persist.sbi;

import com.coin900.coin.persist.FuVO;

/**
 * VO 实体类
 * created by shui
 */
public class SbiSystemConfigVO extends FuVO {

    private String type;

    private String typeDesc;

    private String subtype;

    private String subtypeDesc;

    private String configKey;

    private String env;

    private String value;

    private String description;

    private String alphabetCode;

    private Long sortOrder;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTypeDesc() {
        return typeDesc;
    }

    public void setTypeDesc(String typeDesc) {
        this.typeDesc = typeDesc;
    }

    public String getSubtype() {
        return subtype;
    }

    public void setSubtype(String subtype) {
        this.subtype = subtype;
    }

    public String getSubtypeDesc() {
        return subtypeDesc;
    }

    public void setSubtypeDesc(String subtypeDesc) {
        this.subtypeDesc = subtypeDesc;
    }

    public String getConfigKey() {
        return configKey;
    }

    public void setConfigKey(String configKey) {
        this.configKey = configKey;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAlphabetCode() {
        return alphabetCode;
    }

    public void setAlphabetCode(String alphabetCode) {
        this.alphabetCode = alphabetCode;
    }

    public Long getSortOrder() {
        return sortOrder;
    }

    public void setSortOrder(Long sortOrder) {
        this.sortOrder = sortOrder;
    }

    @Override
    public String toString() {
        return super.toString()
                + "SbiSystemConfigVO{" +
                "type='" + type + '\'' +
                ", typeDesc='" + typeDesc + '\'' +
                ", subtype='" + subtype + '\'' +
                ", subtypeDesc='" + subtypeDesc + '\'' +
                ", configKey='" + configKey + '\'' +
                ", env='" + env + '\'' +
                ", value='" + value + '\'' +
                ", description='" + description + '\'' +
                ", alphabetCode='" + alphabetCode + '\'' +
                ", sortOrder=" + sortOrder +
                '}';
    }
}
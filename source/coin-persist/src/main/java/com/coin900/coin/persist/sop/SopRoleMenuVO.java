package com.coin900.coin.persist.sop;

import com.coin900.coin.persist.FuVO;

/**
* VO 实体类
* created by shui
*/
public class SopRoleMenuVO  extends FuVO {

    private Long roleId;

    private Long menuId;


    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId){
        this.roleId = roleId;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId){
        this.menuId = menuId;
    }

}
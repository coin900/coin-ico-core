package com.coin900.coin.persist.coin;

import com.coin900.coin.persist.FuVO;

import java.util.Date;

/**
 * 订单
 *
 * @author shui
 * @create 2018-1-14
 */
public class CoinOrderVO extends FuVO {

    /**
     * 买方用户 id
     */
    private Long userId;

    /**
     * 项目 id
     */
    private Long projectId;

    /**
     * 订单编号
     */
    private String orderNo;

    /**
     * 支付钱包地址
     */
    private String walletAddress;

    /**
     * 支付方式 虚拟币 id
     */
    private Long payType;

    /**
     * 价格 用于支付的虚拟币个数
     */
    private Long price;

    /**
     * 购买多少个虚拟币
     */
    private Long tokenNumber;

    /**
     * 订单状态 0未支付 1已支付 3退款 4取消
     */
    private Integer orderStatus;

    /**
     * 支付时间
     */
    private Date payDate;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public String getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        this.orderNo = orderNo;
    }

    public Long getPayType() {
        return payType;
    }

    public void setPayType(Long payType) {
        this.payType = payType;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Long getTokenNumber() {
        return tokenNumber;
    }

    public void setTokenNumber(Long tokenNumber) {
        this.tokenNumber = tokenNumber;
    }

    public Integer getOrderStatus() {
        return orderStatus;
    }

    public void setOrderStatus(Integer orderStatus) {
        this.orderStatus = orderStatus;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getWalletAddress() {
        return walletAddress;
    }

    public void setWalletAddress(String walletAddress) {
        this.walletAddress = walletAddress;
    }

    @Override
    public String toString() {
        return super.toString() +
                "CoinOrderVO{" +
                "userId=" + userId +
                ", projectId=" + projectId +
                ", orderNo='" + orderNo + '\'' +
                ", payType=" + payType +
                ", price=" + price +
                ", tokenNumber=" + tokenNumber +
                ", orderStatus=" + orderStatus +
                ", payDate=" + payDate +
                ", walletAddress=" + walletAddress +
                '}';
    }
}
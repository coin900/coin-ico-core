package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.sbi.SbiCityVO;

import java.util.List;

/**
 * 项目中心-项目列表显示-顶部筛选列表
 *
 * @author shui
 * @create 2017-11-28
 **/
public class FuScreenVO {

    List<FuProjectCategoryVO> categoryList;

    List<SbiCityVO> cityList;

    public List<FuProjectCategoryVO> getCategoryList() {
        return categoryList;
    }

    public void setCategoryList(List<FuProjectCategoryVO> categoryList) {
        this.categoryList = categoryList;
    }

    public List<SbiCityVO> getCityList() {
        return cityList;
    }

    public void setCityList(List<SbiCityVO> cityList) {
        this.cityList = cityList;
    }

    @Override
    public String toString() {
        return "FuScreenVO{" +
                "categoryList=" + categoryList +
                ", cityList=" + cityList +
                '}';
    }
}

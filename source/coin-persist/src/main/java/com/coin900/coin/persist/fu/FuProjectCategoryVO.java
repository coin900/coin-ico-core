package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;


/**
 * 项目分类
 *
 * @author shui
 */
public class FuProjectCategoryVO extends FuVO {

    private String categoryName;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuProjectCategoryVO{" +
                "categoryName='" + categoryName + '\'' +
                '}';
    }
}
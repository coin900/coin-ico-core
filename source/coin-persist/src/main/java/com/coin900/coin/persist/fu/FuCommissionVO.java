package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * 佣金
 *
 * @author shui
 * @create 2018-01-02
 */
public class FuCommissionVO extends FuVO {

    /**
     * 受益用户的 id
     */
    private Long userId;

    /**
     * 认筹成功的 id
     */
    private Long fundId;

    /**
     * 认筹金额
     */
    private Long fundAmount;

    /**
     * 佣金
     */
    private Long commission;

    /**
     * 佣金比例
     */
    private Double proportion;

    /**
     * 来源佣金层次 1下级 2下下级
     */
    private Integer level;

    /**
     * 备注
     */
    private String remark;

    /*****非持久化数据开始*****/

    /**
     * 认筹用户手机号
     */

    private String mobile;

    /*****非持久化数据结束*****/

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getFundId() {
        return fundId;
    }

    public void setFundId(Long fundId) {
        this.fundId = fundId;
    }

    public Long getFundAmount() {
        return fundAmount;
    }

    public void setFundAmount(Long fundAmount) {
        this.fundAmount = fundAmount;
    }
    
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Long getCommission() {
        return commission;
    }

    public void setCommission(Long commission) {
        this.commission = commission;
    }

    public Double getProportion() {
        return proportion;
    }

    public void setProportion(Double proportion) {
        this.proportion = proportion;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuCommissionVO{" +
                "userId=" + userId +
                ", fundId=" + fundId +
                ", fundAmount=" + fundAmount +
                ", commission=" + commission +
                ", proportion=" + proportion +
                ", level=" + level +
                ", remark='" + remark + '\'' +
                ", mobile='" + mobile + '\'' +
                '}';
    }
}
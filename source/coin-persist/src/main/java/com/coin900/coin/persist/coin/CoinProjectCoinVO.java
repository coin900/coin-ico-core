package com.coin900.coin.persist.coin;

import com.coin900.coin.persist.FuVO;

/**
 * 项目虚拟币
 *
 * @author shui
 * @create 2018-1-14
 */
public class CoinProjectCoinVO  extends FuVO {

    /**
     * 项目 id
     */
    private Long projectId;

    /**
     * 虚拟币 id
     */
    private Long coinId;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId){
        this.projectId = projectId;
    }

    public Long getCoinId() {
        return coinId;
    }

    public void setCoinId(Long coinId){
        this.coinId = coinId;
    }

}
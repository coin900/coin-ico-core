package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * @author shui
 */
public class FuProjectFundVO extends FuVO {

    /**
     * 用户 id
     */
    private Long userId;

    /**
     * 项目 id
     */
    private Long projectId;

    /**
     * 审核状态 0未通过 1通过 2审核中
     */
    private Integer checkStatus;

    /**
     * 支付状态 0未支付 1已支付
     */
    private Integer payStatus;

    /**
     * 认筹总金额
     */
    private Long fundAmount;

    /**
     * ===== 非持久化数据 start =====
     */

    /**
     * 认筹份数
     */
    private Integer copies;

    /**
     * 基准金额
     */
    private Long baseAmount;

    /**
     * 用户名
     */
    private String username;

    /**
     * 用户头像
     */
    private String portrait;

    /**
     * 项目标题
     */
    private String title;

    /**
     * ===== 非持久化数据 end =====
     */

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public Integer getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(Integer payStatus) {
        this.payStatus = payStatus;
    }

    public Long getFundAmount() {
        return fundAmount;
    }

    public void setFundAmount(Long fundAmount) {
        this.fundAmount = fundAmount;
    }

    public Integer getCopies() {
        return copies;
    }

    public void setCopies(Integer copies) {
        this.copies = copies;
    }

    public Long getBaseAmount() {
        return baseAmount;
    }

    public void setBaseAmount(Long baseAmount) {
        this.baseAmount = baseAmount;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuProjectFundVO{" +
                "userId=" + userId +
                ", projectId=" + projectId +
                ", checkStatus=" + checkStatus +
                ", payStatus=" + payStatus +
                ", fundAmount=" + fundAmount +
                ", copies=" + copies +
                ", baseAmount=" + baseAmount +
                ", username='" + username +
                ", portrait='" + portrait +
                ", title='" + title +
                '}';
    }
}
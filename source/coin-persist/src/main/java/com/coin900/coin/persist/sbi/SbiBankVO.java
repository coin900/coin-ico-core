package com.coin900.coin.persist.sbi;

import com.coin900.coin.persist.FuVO;

/**
 * 银行
 *
 * @author shui
 */
public class SbiBankVO extends FuVO {

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * 用户统计表
 *
 * @author shui
 * @create 2017-11-24
 **/
public class FuUserInfoVO extends FuVO {

    /**
     * 用户 id
     */
    private Long userId;

    /**
     * 总资产
     */
    private Long capital;

    /**
     * 充值审核金额
     */
    private Long capitalRecharge;

    /**
     * 提现冻结金额
     */
    private Long capitalWithdraw;

    /**
     * 虚拟币资产
     */
    private Long virtualCapital;

    /**
     * 虚拟币充值审核金额
     */
    private Long virtualCapitalRecharge;

    /**
     * 虚拟币提现冻结金额
     */
    private Long virtualCapitalWithdraw;

    /**
     * 全部佣金
     */
    private Long totalCommission;

    /**
     * 全部虚拟佣金
     */
    private Long totalVirtualCommission;

    /**
     * 已提现佣金
     */
    private Long withdrawCommission;

    /**
     * 剩余佣金
     */
    private Long remainCommission;

    /**
     * 参与项目个数
     */
    private Long joinNumber;

    /**
     * 认筹项目个数
     */
    private Long fundNumber;

    /**
     * 关注项目个数
     */
    private Long followNumber;

    /**
     * 消息条数
     */
    private Long messageNumber;

    /**
     * 待支付的账单
     */
    private Long unpaidOrderNumber;

    /**
     * 参与项目金额
     */
    private Long projectAmount;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Long getCapital() {
        return capital;
    }

    public void setCapital(Long capital) {
        this.capital = capital;
    }

    public Long getCapitalRecharge() {
        return capitalRecharge;
    }

    public void setCapitalRecharge(Long capitalRecharge) {
        this.capitalRecharge = capitalRecharge;
    }

    public Long getCapitalWithdraw() {
        return capitalWithdraw;
    }

    public void setCapitalWithdraw(Long capitalWithdraw) {
        this.capitalWithdraw = capitalWithdraw;
    }

    public Long getVirtualCapital() {
        return virtualCapital;
    }

    public void setVirtualCapital(Long virtualCapital) {
        this.virtualCapital = virtualCapital;
    }

    public Long getVirtualCapitalRecharge() {
        return virtualCapitalRecharge;
    }

    public void setVirtualCapitalRecharge(Long virtualCapitalRecharge) {
        this.virtualCapitalRecharge = virtualCapitalRecharge;
    }

    public Long getVirtualCapitalWithdraw() {
        return virtualCapitalWithdraw;
    }

    public void setVirtualCapitalWithdraw(Long virtualCapitalWithdraw) {
        this.virtualCapitalWithdraw = virtualCapitalWithdraw;
    }

    public Long getTotalCommission() {
        return totalCommission;
    }

    public void setTotalCommission(Long totalCommission) {
        this.totalCommission = totalCommission;
    }

    public Long getWithdrawCommission() {
        return withdrawCommission;
    }

    public void setWithdrawCommission(Long withdrawCommission) {
        this.withdrawCommission = withdrawCommission;
    }

    public Long getRemainCommission() {
        return remainCommission;
    }

    public void setRemainCommission(Long remainCommission) {
        this.remainCommission = remainCommission;
    }

    public Long getJoinNumber() {
        return joinNumber;
    }

    public void setJoinNumber(Long joinNumber) {
        this.joinNumber = joinNumber;
    }

    public Long getFundNumber() {
        return fundNumber;
    }

    public void setFundNumber(Long fundNumber) {
        this.fundNumber = fundNumber;
    }

    public Long getFollowNumber() {
        return followNumber;
    }

    public void setFollowNumber(Long followNumber) {
        this.followNumber = followNumber;
    }

    public Long getMessageNumber() {
        return messageNumber;
    }

    public void setMessageNumber(Long messageNumber) {
        this.messageNumber = messageNumber;
    }

    public Long getProjectAmount() {
        return projectAmount;
    }

    public void setProjectAmount(Long projectAmount) {
        this.projectAmount = projectAmount;
    }

    public Long getTotalVirtualCommission() {
        return totalVirtualCommission;
    }

    public void setTotalVirtualCommission(Long totalVirtualCommission) {
        this.totalVirtualCommission = totalVirtualCommission;
    }

    public Long getUnpaidOrderNumber() {
        return unpaidOrderNumber;
    }

    public void setUnpaidOrderNumber(Long unpaidOrderNumber) {
        this.unpaidOrderNumber = unpaidOrderNumber;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuUserInfoVO{" +
                "userId=" + userId +
                ", capital=" + capital +
                ", capitalRecharge=" + capitalRecharge +
                ", capitalWithdraw=" + capitalWithdraw +
                ", virtualCapital=" + virtualCapital +
                ", virtualCapitalRecharge=" + virtualCapitalRecharge +
                ", virtualCapitalWithdraw=" + virtualCapitalWithdraw +
                ", totalCommission=" + totalCommission +
                ", totalVirtualCommission=" + totalVirtualCommission +
                ", withdrawCommission=" + withdrawCommission +
                ", remainCommission=" + remainCommission +
                ", joinNumber=" + joinNumber +
                ", fundNumber=" + fundNumber +
                ", followNumber=" + followNumber +
                ", messageNumber=" + messageNumber +
                ", projectAmount=" + projectAmount +
                ", unpaidOrderNumber=" + unpaidOrderNumber +
                '}';
    }
}
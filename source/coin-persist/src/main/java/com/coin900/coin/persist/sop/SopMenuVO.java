package com.coin900.coin.persist.sop;

import com.coin900.coin.persist.FuVO;

/**
 * VO 实体类
 * created by shui
 */
public class SopMenuVO extends FuVO {

    /**
     * 菜单名称
     */
    private String menuName;

    /**
     * 菜单 url
     */
    private String menuUrl;

    /**
     * 父级菜单 id, 顶层为 0
     */
    private Long parentId;

    /**
     * 菜单排序，顶层为 0
     */
    private Integer menuOrder;

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getMenuUrl() {
        return menuUrl;
    }

    public void setMenuUrl(String menuUrl) {
        this.menuUrl = menuUrl;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public Integer getMenuOrder() {
        return menuOrder;
    }

    public void setMenuOrder(Integer menuOrder) {
        this.menuOrder = menuOrder;
    }

    @Override
    public String toString() {
        return super.toString() +
                "SopMenuVO{" +
                "menuName='" + menuName + '\'' +
                ", menuUrl='" + menuUrl + '\'' +
                ", parentId=" + parentId +
                ", menuOrder=" + menuOrder +
                '}';
    }
}
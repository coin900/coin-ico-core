package com.coin900.coin.persist.sop;

import com.coin900.coin.persist.FuVO;

/**
 * VO 实体类
 * created by shui
 */
public class SopButtonVO extends FuVO {

    /**
     * 按钮中文名
     */
    private String name;

    /**
     * 按钮唯一标识
     */
    private String codeName;

    /**
     * 按钮所处菜单 id
     */
    private Long menuId;

    /***** 非持久化数据 START *****/

    /**
     * 菜单名称
     */
    private String menuName;

    /***** 非持久化数据 END *****/

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    @Override
    public String toString() {
        return super.toString() +
                "SopButtonVO{" +
                "name='" + name + '\'' +
                ", codeName='" + codeName + '\'' +
                ", menuId=" + menuId +
                ", menuName=" + menuName +
                '}';
    }
}
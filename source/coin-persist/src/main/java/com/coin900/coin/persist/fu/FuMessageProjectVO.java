package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * 项目消息
 *
 * @author shui
 * @create 2017-11-23
 */
public class FuMessageProjectVO extends FuVO {

    private Long projectId;

    /**
     * 项目类型 1参与的项目 2认筹的项目 3关注的项目
     */
    private Integer projectType;

    private String message;

    /**
     * 0未发送 1已发送
     */
    private Integer status;
    /**
     * ===== 非持久化数据 =====
     */

    /**
     * 项目名称
     */
    private String projectName;

    /**
     * 类型名称：1参与的项目 2认筹的项目 3关注的项目
     */
    private String typeName;

    public Long getProjectId() {
        return projectId;
    }

    public void setProjectId(Long projectId) {
        this.projectId = projectId;
    }

    public Integer getProjectType() {
        return projectType;
    }

    public void setProjectType(Integer projectType) {
        this.projectType = projectType;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return super.toString()
        +"FuMessageProjectVO{" +
                "projectId=" + projectId +
                ", projectType=" + projectType +
                ", message='" + message + '\'' +
                ", status=" + status +
                ", projectName='" + projectName + '\'' +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}
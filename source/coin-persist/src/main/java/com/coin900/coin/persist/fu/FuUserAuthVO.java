package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

/**
 * 用户认证表
 *
 * @author shui
 */
public class FuUserAuthVO extends FuVO {

    /**
     * 用户 id
     */
    private Long userId;

    /**
     * 真实姓名
     */
    private String name;

    /**
     * 身份证号码
     */
    private String idNo;

    /**
     * 正面照片
     */
    private String idCardFacade;

    /**
     * 反面照片
     */
    private String idCardReverse;

    /**
     * 手持身份证
     */
    private String idCardHandheld;

    /**
     * 备注
     */
    private String remark;

    /**
     * 审核状态，1审核通过, 0审核未通过, 2审核中
     */
    private Integer checkStatus;

    /**
     * ******* 非持久化数据 ********
     */

    /**
     * 用户名
     */
    private String username;

    /**
     * ******* 非持久化数据结束 ********
     */

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIdNo() {
        return idNo;
    }

    public void setIdNo(String idNo) {
        this.idNo = idNo;
    }

    public String getIdCardFacade() {
        return idCardFacade;
    }

    public void setIdCardFacade(String idCardFacade) {
        this.idCardFacade = idCardFacade;
    }

    public String getIdCardReverse() {
        return idCardReverse;
    }

    public void setIdCardReverse(String idCardReverse) {
        this.idCardReverse = idCardReverse;
    }

    public String getIdCardHandheld() {
        return idCardHandheld;
    }

    public void setIdCardHandheld(String idCardHandheld) {
        this.idCardHandheld = idCardHandheld;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Integer getCheckStatus() {
        return checkStatus;
    }

    public void setCheckStatus(Integer checkStatus) {
        this.checkStatus = checkStatus;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String toString() {
        return super.toString()
                + "FuUserAuthVO{" +
                "userId=" + userId +
                ", name='" + name + '\'' +
                ", idNo='" + idNo + '\'' +
                ", idCardFacade='" + idCardFacade + '\'' +
                ", idCardReverse='" + idCardReverse + '\'' +
                ", idCardHandheld='" + idCardHandheld + '\'' +
                ", remark='" + remark + '\'' +
                ", checkStatus=" + checkStatus +
                ", username='" + username + '\'' +
                '}';
    }
}
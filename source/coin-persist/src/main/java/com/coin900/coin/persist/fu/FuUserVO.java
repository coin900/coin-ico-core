package com.coin900.coin.persist.fu;

import com.coin900.coin.persist.FuVO;

import java.util.Date;


/**
 * 用户信息
 *
 * @author shui
 */
public class FuUserVO extends FuVO {

    public static final long serialVersionUID = 1L;

    /**
     * 唯一统一标示符
     */
    private String uuid;

    /**
     * 分销中推荐者 id
     */
    private Long parentId;

    /**
     * 1web用户 101sop员工 102 sop领导
     */
    private Integer userType;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 中文名
     */
    private String name;

    /**
     * 头像
     */
    private String portrait;

    /**
     * 性别(1男;2女;0未知)
     */
    private Integer gender;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 邮箱状态，1已验证，0未验证
     */
    private Integer emailStatus;

    /**
     * 出生日期
     */
    private Date birthdate;

    /**
     * qq
     */
    private String qq;

    /**
     * 个性签名
     */
    private String mySign;

    /**
     * 省
     */
    private Long provinceId;

    /**
     * 市
     */
    private Long cityId;

    /**
     * 联系地址
     */
    private String address;

    /**
     * 工作单位
     */
    private String workAddress;

    /**
     * 自我简介
     */
    private String introduction;

    /**
     * 职业
     */
    private String profession;

    /**
     * 用户状态，1可用，0停用
     */
    private Integer userStatus;

    /**
     * 手机认证 0未认证 1已认证
     */
    private Integer moblieStatus;

    /**
     * 实名认证 0未认证 1已认证
     */
    private Integer idcardStatus;

    /**
     * 安全等级1低2中3高
     */
    private Integer safetyLevel;

    /**
     * 同步 key
     */
    private Long syncKey;

    /**
     * 重复密码
     */
    private String repassword;

    /*************非持久化数据 start**************/

    /**
     * 总资产
     */
    private Long capital;

    /**
     * 充值审核金额
     */
    private Long capitalRecharge;

    /**
     * 提现冻结金额
     */
    private Long capitalWithdraw;

    /**
     * 虚拟币资产
     */
    private Long virtualCapital;

    /**
     * 虚拟币充值审核金额
     */
    private Long virtualCapitalRecharge;

    /**
     * 虚拟币提现冻结金额
     */
    private Long virtualCapitalWithdraw;

    /**
     * 参与项目个数
     */
    private Long joinNumber;

    /**
     * 认筹项目个数
     */
    private Long fundNumber;

    /**
     * 关注项目个数
     */
    private Long followNumber;

    /**
     * 消息条数
     */
    private Long messageNumber;

    /**
     * 待支付的账单
     */
    private Long unpaidOrderNumber;


    private String bucketDomain;


    private String upToken;

    /*************非持久化数据 end**************/


    public String getUuid() {
        return uuid;
    }

    public Integer getUserType() {
        return userType;
    }

    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(Integer emailStatus) {
        this.emailStatus = emailStatus;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getMySign() {
        return mySign;
    }

    public void setMySign(String mySign) {
        this.mySign = mySign;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getProfession() {
        return profession;
    }

    public void setProfession(String profession) {
        this.profession = profession;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getSafetyLevel() {
        return safetyLevel;
    }

    public void setSafetyLevel(Integer safetyLevel) {
        this.safetyLevel = safetyLevel;
    }

    public Long getSyncKey() {
        return syncKey;
    }

    public void setSyncKey(Long syncKey) {
        this.syncKey = syncKey;
    }

    public Long getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Long provinceId) {
        this.provinceId = provinceId;
    }

    public Long getCityId() {
        return cityId;
    }

    public void setCityId(Long cityId) {
        this.cityId = cityId;
    }

    public String getRepassword() {
        return repassword;
    }

    public void setRepassword(String repassword) {
        this.repassword = repassword;
    }

    public Integer getMoblieStatus() {
        return moblieStatus;
    }

    public void setMoblieStatus(Integer moblieStatus) {
        this.moblieStatus = moblieStatus;
    }

    public Integer getIdcardStatus() {
        return idcardStatus;
    }

    public void setIdcardStatus(Integer idcardStatus) {
        this.idcardStatus = idcardStatus;
    }

    public String getBucketDomain() {
        return bucketDomain;
    }

    public void setBucketDomain(String bucketDomain) {
        this.bucketDomain = bucketDomain;
    }

    public Long getCapital() {
        return capital;
    }

    public void setCapital(Long capital) {
        this.capital = capital;
    }

    public Long getCapitalRecharge() {
        return capitalRecharge;
    }

    public void setCapitalRecharge(Long capitalRecharge) {
        this.capitalRecharge = capitalRecharge;
    }

    public Long getCapitalWithdraw() {
        return capitalWithdraw;
    }

    public void setCapitalWithdraw(Long capitalWithdraw) {
        this.capitalWithdraw = capitalWithdraw;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getVirtualCapital() {
        return virtualCapital;
    }

    public void setVirtualCapital(Long virtualCapital) {
        this.virtualCapital = virtualCapital;
    }

    public Long getVirtualCapitalRecharge() {
        return virtualCapitalRecharge;
    }

    public void setVirtualCapitalRecharge(Long virtualCapitalRecharge) {
        this.virtualCapitalRecharge = virtualCapitalRecharge;
    }

    public Long getVirtualCapitalWithdraw() {
        return virtualCapitalWithdraw;
    }

    public void setVirtualCapitalWithdraw(Long virtualCapitalWithdraw) {
        this.virtualCapitalWithdraw = virtualCapitalWithdraw;
    }

    public String getUpToken() {
        return upToken;
    }

    public void setUpToken(String upToken) {
        this.upToken = upToken;
    }

    public Long getJoinNumber() {
        return joinNumber;
    }

    public void setJoinNumber(Long joinNumber) {
        this.joinNumber = joinNumber;
    }

    public Long getFundNumber() {
        return fundNumber;
    }

    public void setFundNumber(Long fundNumber) {
        this.fundNumber = fundNumber;
    }

    public Long getFollowNumber() {
        return followNumber;
    }

    public void setFollowNumber(Long followNumber) {
        this.followNumber = followNumber;
    }

    public Long getMessageNumber() {
        return messageNumber;
    }

    public void setMessageNumber(Long messageNumber) {
        this.messageNumber = messageNumber;
    }

    public Long getUnpaidOrderNumber() {
        return unpaidOrderNumber;
    }

    public void setUnpaidOrderNumber(Long unpaidOrderNumber) {
        this.unpaidOrderNumber = unpaidOrderNumber;
    }

    @Override
    public String toString() {
        return super.toString() +
                "FuUserVO{" +
                "uuid='" + uuid + '\'' +
                ", parentId=" + parentId +
                ", userType=" + userType +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", portrait='" + portrait + '\'' +
                ", gender=" + gender +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                ", emailStatus=" + emailStatus +
                ", birthdate=" + birthdate +
                ", qq='" + qq + '\'' +
                ", mySign='" + mySign + '\'' +
                ", provinceId=" + provinceId +
                ", cityId=" + cityId +
                ", address='" + address + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", introduction='" + introduction + '\'' +
                ", profession='" + profession + '\'' +
                ", userStatus=" + userStatus +
                ", moblieStatus=" + moblieStatus +
                ", idcardStatus=" + idcardStatus +
                ", safetyLevel=" + safetyLevel +
                ", syncKey=" + syncKey +
                ", repassword='" + repassword + '\'' +
                ", capital=" + capital +
                ", capitalRecharge=" + capitalRecharge +
                ", capitalWithdraw=" + capitalWithdraw +
                ", virtualCapital=" + virtualCapital +
                ", virtualCapitalRecharge=" + virtualCapitalRecharge +
                ", virtualCapitalWithdraw=" + virtualCapitalWithdraw +
                ", joinNumber=" + joinNumber +
                ", fundNumber=" + fundNumber +
                ", followNumber=" + followNumber +
                ", messageNumber=" + messageNumber +
                ", unpaidOrderNumber=" + unpaidOrderNumber +
                ", bucketDomain='" + bucketDomain + '\'' +
                ", upToken='" + upToken + '\'' +
                '}';
    }
}
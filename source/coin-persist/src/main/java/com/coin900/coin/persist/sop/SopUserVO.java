package com.coin900.coin.persist.sop;

import com.coin900.coin.persist.FuVO;

/**
 * 运维人员
 *
 * @author shui
 * @create 2018-01-06
 **/
public class SopUserVO extends FuVO {

    /**
     * 角色 id
     */
    private Long roleId;

    /**
     * 用户名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 中文名
     */
    private String name;

    /**
     * 头像
     */
    private String portrait;

    /**
     * 性别(1男;2女;0未知)
     */
    private Integer gender;

    /**
     * 手机号
     */
    private String mobile;

    /**
     * 邮箱号
     */
    private String email;

    /**
     * 用户状态，1可用，0停用
     */
    private Integer userStatus;

    /**
     * 邮箱状态，1已验证，0未验证
     */
    private Integer emailStatus;

    /**
     * 手机认证 0未认证 1已认证
     */
    private Integer moblieStatus;

    /***** 非持久化数据开始 *****/

    /**
     * 角色名
     */
    private String roleName;

    /***** 非持久化数据结束 *****/

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPortrait() {
        return portrait;
    }

    public void setPortrait(String portrait) {
        this.portrait = portrait;
    }

    public Integer getGender() {
        return gender;
    }

    public void setGender(Integer gender) {
        this.gender = gender;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(Integer userStatus) {
        this.userStatus = userStatus;
    }

    public Integer getEmailStatus() {
        return emailStatus;
    }

    public void setEmailStatus(Integer emailStatus) {
        this.emailStatus = emailStatus;
    }

    public Integer getMoblieStatus() {
        return moblieStatus;
    }

    public void setMoblieStatus(Integer moblieStatus) {
        this.moblieStatus = moblieStatus;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Override
    public String toString() {
        return super.toString() +
                "SopUserVO{" +
                "roleId=" + roleId +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", portrait='" + portrait + '\'' +
                ", gender=" + gender +
                ", mobile='" + mobile + '\'' +
                ", email='" + email + '\'' +
                ", userStatus=" + userStatus +
                ", emailStatus=" + emailStatus +
                ", moblieStatus=" + moblieStatus +
                ", roleName=" + roleName +
                '}';
    }
}